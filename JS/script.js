// Basic settings
Element.prototype.addClass = function(c) {
  var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g")
  if (re.test(this.className)) return
  this.className = (this.className + " " + c).replace(/\s+/g, " ")
    .replace(/(^ | $)/g, "")
}
Element.prototype.removeClass = function(c) {
  var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g")
  this.className = this.className.replace(re, "$1").replace(/\s+/g, " ")
    .replace(/(^ | $)/g, "")
}

var fixMenu = function() {
  var navbar = document.querySelector('.header nav')
  window.pageYOffset >= 155 && navbar.addClass('fixed');
  window.pageYOffset < 155 && navbar.removeClass('fixed');
}
function getXmlHttp() {
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  }
  catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}

// Main DOM manipulate
window.addEventListener('load', function() {
  var slideout = new Slideout({
    'panel': document.getElementById('full-content'),
    'menu': document.getElementById('navbar'),
    'padding': 256,
    'tolerance': 70
  });
  if (window.location.pathname == '/') {
    var xhr = getXmlHttp();
    var timePlace = document.getElementById('time');
    var getTime = function() {
      xhr.open("GET", "/time.json");
      xhr.send();
      xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            var nextDate = new Date(JSON.parse(xhr.responseText).date);
            var today = new Date();
            var diff = nextDate - new Date();
            var days = Math.floor(diff / 1000 / 60 / 60 / 24);
            var hours = Math.floor((diff - days * 86400000) / 1000 / 60 / 60);
            var minutes = Math.floor((diff - days * 86400000 - hours * 3600000) / 1000 / 60);
            console.log(days, hours, minutes, timePlace);
            timePlace.innerHTML = days + 'д ' + hours + 'ч ' + minutes + 'м';
          }
        }
      };
    }
    getTime();
    setInterval(getTime, 15000);
  }
  document.getElementsByTagName('body')[0].addClass('active');
  fixMenu();
})

window.addEventListener('scroll', fixMenu)